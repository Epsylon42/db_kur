#!/usr/bin/python3

import sys
import re

from common import connect, publish

if len(sys.argv) > 1:
    day = sys.argv[1]
else:
    day = 'default'

add = '--noadd' not in sys.argv

pattern = re.compile('(.+) IP (.+)\\.([0-9a-zA-Z\\-_]+) > (.+)\\.([0-9a-zA-Z\\-_]+):.*')
rc = connect()


buf = ''
while True:
    c = sys.stdin.read(1)
    if c == '\n':
        match = pattern.match(buf)
        if match is not None:
            time = int(match.group(1).replace(':', '').replace('.', ''))
            ip1 = match.group(2)
            port1 = match.group(3)
            ip2 = match.group(4)
            port2 = match.group(5)
            print('from', ip1, 'to', ip2, flush=True)

            p = { 'ip_from': ip1, 'ip_to': ip2, 'port_from': port1, 'port_to': port2 }
            if add:
                publish(rc, p, time, day)
        buf = ''
    else:
        buf += c
