#!/bin/bash

for dir in 700*; do
    cd $dir
    redis-server redis.conf&
    cd ..
done
