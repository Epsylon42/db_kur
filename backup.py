#!/usr/local/bin/python3

import os
import sys
import common

from datetime import datetime

now = datetime.now().strftime('%Y-%m-%dT%H:%M:%S')

try:
    rc = common.connect()
except:
    rc = None

rsync = ' rsync --recursive --progress --include "dump.rdb" --include "*/" --exclude "*" '

if sys.argv[1] == 'backup':
    if rc is not None:
        rc.save()
    os.system("%s 'cluster' 'backups/%s'" % (rsync, now))
elif sys.argv[1] == 'restore':
    os.system("%s 'backups/%s/cluster' '.'" % (rsync, sys.argv[2]))
else:
    print('Unknown command')
