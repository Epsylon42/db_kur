#!/usr/local/bin/python3.7

import sys
import argparse as ap

import matplotlib
matplotlib.use('Qt5Cairo')
import matplotlib.pyplot as plt

import numpy as np
import scipy.stats as stats

from uuid import uuid4

import common


def get(addr_from, addr_to, time_from, time_to, day):
    if time_from is None:
        time_from = 0
    if time_to is None:
        time_to = 999999999999

    packets = str(uuid4())
    rc.sadd(packets, *rc.zrangebyscore('packet:{by-time:%s}' % day, time_from, time_to))
    rc.expire(packets, 240)

    temp = str(uuid4())

    if addr_from is not None:
        addrs = rc.keys('packet:{from:%s}' % addr_from)
        rc.sunionstore(temp, *addrs)
        rc.expire(temp, 240)

        rc.sinterstore(packets, packets, temp)

    if addr_to is not None:
        addrs = rc.keys('packet:{to:%s}' % addr_to)
        rc.sunionstore(temp, *addrs)
        rc.expire(temp, 240)

        rc.sinterstore(packets, packets, temp)

    packets_arr = [common.get_time(x) for x in rc.smembers(packets)]
    rc.delete(packets)
    packets_arr.sort()

    return packets_arr[1:-2]


def hosts(time_from, time_to, day, count=None):
    if time_from is None:
        time_from = 0
    if time_to is None:
        time_to = 999999999999

    hosts_from = list(rc.keys('packet:{by-time:%s:from:*}' % day))
    hosts_to = list(rc.keys('packet:{by-time:%s:to:*}' % day))

    p = rc.pipeline()

    for host in hosts_from:
        p.zcount(host, time_from, time_to)
    f = [(common.get_host(host), count) for host, count in zip(hosts_from, p.execute()) if count != 0]

    for host in hosts_to:
        p.zcount(host, time_from, time_to)
    t = [(common.get_host(host), count) for host, count in zip(hosts_to, p.execute()) if count != 0]

    t.sort(reverse=True, key=lambda pair: pair[1])
    f.sort(reverse=True, key=lambda pair: pair[1])

    if count is not None:
        t = t[:count]
        f = f[:count]

    print('from:')
    for host, card in f:
        print('%s: %s' % (host, card))
    print('\nto:')
    for host, card in t:
        print('%s: %s' % (host, card))


def flow(a, b, addr, time_from, time_to, day, count=None):
    if time_from is None:
        time_from = 0
    if time_to is None:
        time_to = 999999999999

    packets = rc.zrangebyscore('packet:{by-time:%s:%s:%s}' % (day, a, addr), time_from, time_to)
    p = rc.pipeline();
    for key in packets:
        p.hget(key, b)
    
    counts = {}
    for host in p.execute():
        if host not in counts:
            counts[host] = 1
        else:
            counts[host] += 1

    counts = [(host, count) for host, count in counts.items()]
    counts.sort(reverse=True, key=lambda pair: pair[1])

    if count is not None:
        counts = counts[:count]
    
    for host, count in counts:
        print('%s: %s' % (host, count))


if __name__ == '__main__':
    p = ap.ArgumentParser()
    p.add_argument('command', type=str, default='vis')
    p.add_argument('--addr-from', type=str)
    p.add_argument('--addr-to', type=str)
    p.add_argument('--time-from', type=str)
    p.add_argument('--time-to', type=str)
    p.add_argument('--day', type=str, default='default')
    p.add_argument('--count', type=int)

    args = p.parse_args()

    rc = common.connect()

    time_from = int(common.convert_time(args.time_from)) if args.time_from is not None else None
    time_to = int(common.convert_time(args.time_to)) if args.time_to is not None else None

    if args.command in ['vis', 'regression']:
        packets = get(args.addr_from, args.addr_to, time_from, time_to, args.day)
        time_from = min(packets)
        time_to = max(packets)
        if args.command == 'vis':
            plt.hist(packets, bins=100)
            plt.show()
        else:
            plt.hist(packets, bins=100)

            bin_size = (int(time_to) - int(time_from)) / 100
            bins = [0]*100
            for packet in packets:
                index = int((packet - time_from) / (time_to - time_from) * 100)
                bins[min(index, 99)] += 1
            slope, intercept, *_ = stats.linregress([time_from+bin_size*x for x in range(100)], bins)

            x = np.linspace(time_from, time_to, num=100)
            y = x * slope + intercept
            print(slope, intercept)
            plt.plot(x, y)

            plt.show()
    elif args.command == 'hosts':
        hosts(time_from, time_to, args.day, args.count)
    elif args.command == 'flow':
        if args.addr_from is None and args.addr_to is None:
            print('You need to specify either --addr-from or --addr-to', file=sys.stderr)
        elif args.addr_from is not None and args.addr_to is not None:
            print("Can't specify both --addr-from and --addr-to", file=sys.stderr)
        else:
            a, b, addr = ('from', 'to', args.addr_from) if args.addr_from is not None else ('to', 'from', args.addr_to)
            flow(a, b, addr, time_from, time_to, args.day, args.count)
    else:
        print('Unknown command', file=sys.stderr)

