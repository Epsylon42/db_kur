import sys
from common import connect

if len(sys.argv) > 1:
    day = sys.argv[1]
else:
    day = 'default'

rc = connect()

ps = rc.pubsub()
ps.subscribe('packet:{unprocessed:%s}' % day)
print('listeling')
for msg in ps.listen():
    if msg['type'] != 'message':
        continue

    key = msg['data']
    print(key)
    ip_from = rc.hget(key, 'from')
    ip_to = rc.hget(key, 'to')
    time = rc.hget(key, 'time')
    dash = time.find('-')
    if dash > 0:
        time = int(time[:dash])
    else:
        time = int(time)

    rc.sadd('packet:{from:%s}' % ip_from, key)
    rc.sadd('packet:{to:%s}' % ip_to, key)

    rc.zincrby('packet:{by-time:%s}' % day, time, key)
    rc.zincby('packet:{by-time:%s:from:%s}' % (day, ip_from), time, key)
    rc.zincby('packet:{by-time:%s:from:%s}' % (day, ip_from), time, key)
