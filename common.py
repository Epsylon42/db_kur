from random import random
from datetime import datetime
from rediscluster import RedisCluster

def connect():
    rc = RedisCluster(startup_nodes=[{'host': 'localhost', 'port': str(x)} for x in range(7000, 7007)], decode_responses=True)
    return rc

def publish(rc, p, time=None, day='default'):
    if time is None:
        time = datetime.now().strftime('%H%M%S%f')
    key = 'packet:{%s-%s}' % (time, int(random() * 1000))

    rc.hset(key, 'from', p['ip_from'])
    rc.hset(key, 'to', p['ip_to'])
    rc.hset(key, 'fport', p['port_from'])
    rc.hset(key, 'tport', p['port_to'])
    rc.hset(key, 'time', time)
    rc.publish('packet:{unprocessed:%s}' % day, key)

def get_time(key):
    brace = key.find('{')
    dash = key.find('-')
    num = key[brace+1:dash]
    return int(num)

def pad_front(s, n, placeholder):
    if len(s) < n:
        return placeholder*(n-len(s)) + s
    else:
        return s

def pad_back(s, n, placeholder):
    if len(s) < n:
        return s + placeholder*(n-len(s))
    else:
        return s

def convert_time(time):
    if time is None:
        return None

    parts = time.split('.')
    if len(parts) > 1:
        u = pad_front(parts[1], 6)
        time = parts[0]
    else:
        u = '0'*6

    h, m, s = pad_back([pad_front(s, 2, '0') for s in time.split(':')], 3, ['00'])
    return ''.join([h, m, s, u])

def get_host(key):
    return key.split(':')[-1][:-1]
